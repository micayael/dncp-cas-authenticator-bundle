<?php

namespace Dncp\CasAuthenticatorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class DncpCasAuthenticatorExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $authenticator = $container->register('dncp.cas.authenticator',
            'Dncp\CasAuthenticatorBundle\Security\CasAuthenticator');
        $authenticator->setArguments([$config]);

        $provider = $container->register('dncp.cas.user_provider',
            'Dncp\CasAuthenticatorBundle\Security\User\CasUserProvider');
        $provider->setArguments([$config]);

        $logoutHandler = $container->register('dncp.cas.logout_success_handler',
            'Dncp\CasAuthenticatorBundle\Security\LogoutSuccessHandler');
        $logoutHandler->setArguments([$config]);
    }
}
