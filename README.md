# CasAuthBundle
Basado en https://github.com/PRayno/CasAuthBundle

Basic CAS (SSO) authenticator for Symfony 3 and 4

This bundle provides a -very- basic CAS (http://jasig.github.io/cas/4.1.x/index.html) authentication client for Symfony 3 and 4.

## Installation

Install the library via [Composer](https://getcomposer.org/) by
running the following command:

```bash
composer require dncp/cas-authenticator-bundle
```

Next, enable the bundle in your `app/AppKernel.php` file:

```php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Dncp\CasAuthenticatorBundle\DncpCasAuthenticatorBundle(),
        // ...
    );
}
```

In config.yml (Symfony 3) or config/packages/dncp_cas_authenticator.yaml (create this file in Symfony 4), add these settings :
```yaml
dncp_cas_authenticator:
    server_login_url: https://mycasserver/cas/
    server_validation_url: https://mycasserver/cas/serviceValidate
    server_logout_url: https://mycasserver/cas/logout
    
    # Redirección después del logout
    after_logout_url: http://my-app/url
    
    # URI para obtener los permisos de un usuario
    get_permissions_url: http://webservices-ws/seguridad/perfiles/{username}?modulo=pbc
    # URI para obtener los datos del usuario
    get_user_data_url: http://webservices-ws/seguridad/usuario/{username}
    
    xml_namespace: cas
    options:[] see http://docs.guzzlephp.org/en/latest/request-options.html
```
Note : the xml_namespace and options parameters are optionals

Modify your security.yml with the following values (the provider in the following settings should not be used as it's just a very basic example ; in production, create your own UserProvider and add its service name in providers:cas:id) :
```yaml
security:
    providers:
        cas:
          id: dncp.cas_user_provider

    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            anonymous: ~
            logout:
                path: /logout
                success_handler: dncp.logout_success_handler
            guard:
                authenticators:
                    - dncp.cas_authenticator

    access_control:
        - { path: ^/, roles: ROLE_USER }
  ```
  
Agregar la ruta para el logout

```yaml
logout:
   path: /logout
```
