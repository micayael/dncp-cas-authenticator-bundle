<?php

namespace Dncp\CasAuthenticatorBundle\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    private $logoutUrl;
    private $afterLogoutUrl;
    private $options;

    public function __construct(array $config)
    {
        $this->logoutUrl = $config['server_logout_url'];
        $this->afterLogoutUrl = $config['after_logout_url'];
        $this->options = $config['options'];
    }

    public function onLogoutSuccess(Request $request)
    {
        $url = $this->logoutUrl.'?service='.$this->afterLogoutUrl;

        return new RedirectResponse($url);
    }
}
