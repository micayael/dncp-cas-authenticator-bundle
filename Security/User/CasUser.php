<?php

namespace Dncp\CasAuthenticatorBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

class CasUser implements UserInterface
{
    private $username;
    private $password;
    private $salt;
    private $roles;

    private $data;

    /**
     * @param string $username
     * @param string $password
     * @param string $salt
     * @param array  $roles
     * @param array  $data
     */
    public function __construct($username, $password, $salt, array $roles, array $data)
    {
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
        $this->data = $data;
    }

    public function isConvocante()
    {
        if (isset($this->data['convocante']) && in_array('ROLE_CONVOCANTE', $this->roles)) {
            return true;
        }

        return false;
    }

    public function isFuncionarioDncp()
    {
        if (array_key_exists('funcionario', $this->data) && in_array('ROLE_DNCP', $this->roles)) {
            return true;
        }

        return false;
    }

    public function __call($name, $arguments)
    {
        if (false !== strpos($name, 'getConvocante')) {
            $key = strtolower(substr($name, strlen('getConvocante')));

            if (!$this->isConvocante()) {
                throw new \Exception('El usuario no es una convocante');
            }

            if (!isset($this->data['convocante'][$key])) {
                throw new \Exception('No existe la propiedad: '.$key);
            }

            return $this->data['convocante'][$key];
        }

        throw new \Exception('No existe método: '.$name);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return CasUser
     */
    public function setData(array $data): CasUser
    {
        $this->data = $data;

        return $this;
    }
}
