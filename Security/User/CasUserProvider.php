<?php

namespace Dncp\CasAuthenticatorBundle\Security\User;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class CasUserProvider implements UserProviderInterface
{
    private $getPermissionsUrl;
    private $getUserDataUrl;
    private $options;

    public function __construct(array $config)
    {
        $this->getPermissionsUrl = $config['get_permissions_url'];
        $this->getUserDataUrl = $config['get_user_data_url'];
        $this->options = $config['options'];
    }

    /**
     * Provides the authenticated user a ROLE_USER.
     *
     * @param $username
     *
     * @return CasUser
     *
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        if ($username) {
            $password = '...';
            $salt = '';
            $roles = ['ROLE_USER'];

            if ($this->getPermissionsUrl) {
                $url = str_replace('{username}', $username, $this->getPermissionsUrl);

                $client = new Client();
                $response = $client->request('GET', $url, $this->options);

                if (Response::HTTP_OK !== $response->getStatusCode()) {
                    throw new \Exception(
                        sprintf('No fue posible obtener los permisos para el usuario %s (%s).', $username)
                    );
                }

                $allRoles = json_decode($response->getBody(), true);

                $url = str_replace('{username}', $username, $this->getUserDataUrl);

                $response = $client->request('GET', $url, $this->options);

                if (Response::HTTP_OK !== $response->getStatusCode()) {
                    throw new \Exception(
                        sprintf('No fue posible obtener los datos del usuario %s (%s).', $username)
                    );
                }

                $userData = json_decode($response->getBody(), true);

                foreach ($allRoles as $role) {
                    $roles[] = 'ROLE_'.strtoupper($role['codigo']);
                }
            }

            return new CasUser($username, $password, $salt, $roles, $userData);
        }

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    /**
     * @param UserInterface $user
     *
     * @return CasUser
     *
     * @throws UnsupportedUserException
     * @throws UsernameNotFoundException
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof CasUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return 'Dncp\CasAuthenticatorBundle\Security\User\CasUser' === $class;
    }
}
